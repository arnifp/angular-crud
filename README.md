# AngularCrud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Applied Angular Material
Angular material was applied

# Unit tests applied to the product pomponent
## Init: should load the products
This test verifies that the products are loaded.

## Should call the server to remove a product
This test verifies that the deleteProduct method of the service is called.

## Should open the modal and call the service method.
This test verifies that it shows the confirmation modal and then a product is removed.

# Applied Models

## Product model
An interface was applied as a model for products