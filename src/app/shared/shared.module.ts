import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { FormDialogComponent } from './components/form-dialog/form-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [ MaterialModule ],
  declarations: [
    DeleteDialogComponent,
    FormDialogComponent
  ]
})
export class SharedModule {
}
