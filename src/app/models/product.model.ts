export interface Product {
  id?: string;
  code: number;
  name: string;
  description: string;
}
