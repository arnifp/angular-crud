export interface FormField {
  type: string;
  placeholder: string;
  name: string;
  isRequired: boolean;
  label: string;
  isReadonly?: boolean;
  isHidden?: boolean;
}

export interface FormDataComponent {
  title: string;
  formGroup: any;
  successAction?: Function;
  cancelAction?: Function;
  isEditForm: boolean;
  loadingAction?: boolean;
  fields: FormField[]
}