import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Product } from 'src/app/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private resource: string;
  private productsCollection: AngularFirestoreCollection<Product> | undefined;

  constructor(public afs: AngularFirestore | null) {
    this.resource = 'products'
    // this.productsCollection = afs?.collection<Product>(this.resource);
  }

  public getProducts() {
    return this.afs?.collection<Product>(this.resource, ref => {
      return ref.orderBy('name', 'asc');
    }).valueChanges();
  }

  // public getProduct(productId: string) {
  //   return this.afs?.collection<Product>(this.resource, ref => {
  //     return ref.where('id', '==', productId);
  //   }).valueChanges();
  // }

  // public addNewProduct(product: Product) {
  //   product.id = this.afs?.createId();
  //   const normalized = Object.assign({}, product);
  //   return this.productsCollection?.doc(product.id).set(normalized);
  // }

  // public updateProduct(product: Product) {
  //   return this.afs?.doc<Product>(`${this.resource}/${product.id}`).update(product);
  // }

  public deleteProduct(productId: string) {
    return this.afs?.doc<Product>(`${this.resource}/${productId}`).delete();
  }
}
