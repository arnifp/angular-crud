import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/shared/components/delete-dialog/delete-dialog.component';
import { FormDialogComponent } from 'src/app/shared/components/form-dialog/form-dialog.component';
import { first } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ProductService } from 'src/app/services/product/product.service';
import { Product } from 'src/app/models/product.model';
import { FormDataComponent } from 'src/app/models/form-data-component.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {
  public productFormGroup!: FormGroup;
  public displayedColumns: string[] = ['code', 'name', 'description', 'actions'];
  public products!: Product[];
  public dataSource!: MatTableDataSource<Product>;
  public messageAcction: string;
  public successAction: boolean;
  private $subscription: Subscription = new Subscription;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(public _productS: ProductService,
    public dialog: MatDialog,
    public formBuilder: FormBuilder) {
    this.messageAcction = ''
    this.successAction = true;
  }

  ngOnInit(): void {
    this.loadProducts();
    // this.createProductForm()
  }

  // private createProductForm() {
  //   this.productFormGroup = this.formBuilder.group({
  //     id: new FormControl(''),
  //     name: new FormControl(null, Validators.required),
  //     code: new FormControl(null, Validators.required),
  //     description: new FormControl(null, Validators.required),
  //   });
  // }
  
  private loadProducts() {
    this._productS.getProducts()?.subscribe(resp => {
      this.dataSource = new MatTableDataSource<Product>(resp);
      this.dataSource.paginator = this.paginator;
    });
  }

  public deleteProduct(productId: string) {
    const dialogDeleteRef = this.dialog.open(DeleteDialogComponent);

    dialogDeleteRef.afterClosed().pipe(first()).subscribe(result => {
      if (result) {
        try {
          this._productS.deleteProduct(productId);
          this.successAction = true;
          this.messageAcction = 'Se elimino el registro satisfactoriamente!';
        } catch (error) {
          this.messageAcction = 'Error!!! No se pudo completar la acción.';
          this.successAction = false;
          console.log(error);
        } finally {
          setTimeout(() => this.messageAcction = '', 3000);
        }
      }
    });
  }

  // public addNewProduct() {
  //   this.productFormGroup.reset();
  //   const data: FormDataComponent = {
  //     title: 'Crear Nuevo Producto',
  //     formGroup: this.productFormGroup,
  //     loadingAction: false,
  //     isEditForm: false,
  //     fields: [
  //       {
  //         label: 'Nombre',
  //         type: 'text',
  //         placeholder: 'Nombre del producto',
  //         name: 'name',
  //         isRequired: true,
  //       },
  //       {
  //         label: 'Código',
  //         type: 'number',
  //         placeholder: 'Ejem: 1',
  //         name: 'code',
  //         isRequired: true,
  //       },
  //       {
  //         label: 'Descripción',
  //         type: 'text',
  //         placeholder: 'Descripción',
  //         name: 'description',
  //         isRequired: true,
  //       }
  //     ]
  //   }
  //   const dialogFormRef = this.dialog.open(FormDialogComponent, { data });

  //   dialogFormRef.afterClosed().pipe(first()).subscribe(async (result) => {
  //     if (result) {
  //       try {
  //         this._productS.addNewProduct(this.productFormGroup.value);
  //         this.successAction = true;
  //         this.messageAcction = 'Acción realizada exitosamente!';
  //       } catch (error) {
  //         console.log(error);
  //         this.successAction = false;
  //         this.messageAcction = 'Error!!! No se pudo completar la acción.';
  //       } finally {
  //         setTimeout(() => this.messageAcction = '', 3000);
  //       }
  //     }
  //   });
  // }

  ngOnDestroy(): void {
    if (this.$subscription) {
      this.$subscription.unsubscribe()
    }
  }
}
