import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product/product.service';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ProductComponent } from './product.component';

class MatDialogMock {
  open() {
    return {
      afterClosed: () => of({action: true})
    };
  }
};

describe('ProductComponent', () => {
  let component: ProductComponent;
  let fixture: ComponentFixture<ProductComponent>;
  const service: ProductService = new ProductService(null);
  const matDialog = new MatDialogMock();
  let debugElement: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductComponent ],
      imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: ProductService, useValue: service },
        { provide: MatDialog, useValue: matDialog },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Init: should load the products', () => {
    const fakeProducts: Product[] = [
      { id: '', code: 1, name: 'Producto uno', description: 'Mi descripción' }
    ];
    
    spyOn(service, 'getProducts').and.returnValue(of(fakeProducts));
    component.ngOnInit();

    expect(component.dataSource.data.length).toBeGreaterThan(0);
  });

  it('should call the server to remove a product', () => {
    const deleteSpy  = spyOn(service, 'deleteProduct').and.returnValue(Promise.resolve());
    component.deleteProduct('1');

    expect(deleteSpy).toHaveBeenCalledWith('1');
  });

  it('Should open the modal and call the service method', () => {
    spyOn(matDialog, 'open').and.callThrough();
    const deleteSpy  = spyOn(service, 'deleteProduct').and.returnValue(Promise.resolve());

    component.deleteProduct('1');
    expect(deleteSpy).toHaveBeenCalledWith('1');
  });
});
