// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAzflFVjsC8t8NnTuvgipwPpdalHy6ML0o",
    authDomain: "onlyfortest-56972.firebaseapp.com",
    databaseURL: "https://onlyfortest-56972.firebaseio.com",
    projectId: "onlyfortest-56972",
    storageBucket: "onlyfortest-56972.appspot.com",
    messagingSenderId: "35171900771",
    appId: "1:35171900771:web:bb039773c2a764cfeb8756"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
